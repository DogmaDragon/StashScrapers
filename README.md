# StashScrapers for PMVs

[Stash](https://github.com/stashapp/stash) scrapers oriented around PMV metadata where studio is the creator.

More scrapers are available on [community repo](https://github.com/stashapp/CommunityScrapers).

